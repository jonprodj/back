package com.test.demo.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Column;
import java.util.Objects;

@Entity
@Table(name = "users")
public class Users {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name = "id")
  private Long id;
  @Column(name = "name")
  private String name;
  @Column(name = "age")
  private String age;
  @Column(name = "nick")
  private String nick;
  @Column(name = "city")
  private String city;
  @Column(name = "password")
  private String password;
  private String token;
  private Boolean logged;

  public Users() {}
  public Users(String nick, String password){
        this.nick = nick;
        this.password = password;
        this.logged = false;
  }


  @Override
  public String toString() {
    return String.format(
        "User[id=%d, name='%s', nick='%s', password='%s']",
        id, name, nick,password);
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getNick() {
    return nick;
  }
  public void setNick(String nick) {
    this.nick = nick;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public String getToken() {
    return token;
  }

  public void setToken(String token) {
      this.token = token;
  }
  public Boolean getLogged() {
      return logged;
  }

  public void setLogged(Boolean logged) {
      this.logged = logged;
  }
  @Override
  public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof Users)) return false;
      Users user = (Users) o;
      return Objects.equals(nick, user.nick) &&
              Objects.equals(password, user.password);
  }
}