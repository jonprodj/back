package com.test.demo.model;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.test.demo.model.Users;
@Repository
public interface UserRepository extends JpaRepository<Users, Long> {
}