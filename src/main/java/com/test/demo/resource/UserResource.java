package com.test.demo.resource;

import com.test.demo.service.dto.User;
import com.test.demo.model.UserRepository;
import com.test.demo.model.Users;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.core.GrantedAuthority;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

@RestController
public class UserResource {
    @Autowired
    UserRepository userRepository;
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    private EntityManager em;

    @PostMapping("user")
    public Users login(@RequestParam("username") String username, @RequestParam("password") String pwd) {

        /* Here query valid user and password */
        Users user = new Users();
        user.setNick(username);
        user.setPassword(pwd);
        List<Users> users = userRepository.findAll();
        System.err.println(users);
        for (Users other : users) {
            System.err.println(users);
            if (other.equals(user)) {
                String token = getJWTToken(username);
                user.setToken(token);
                user.setLogged(true);
            }else{
                user.setLogged(false);
            }
        }
        return user;     

    }

    private String getJWTToken(String username) {
        String secretKey = "mySecretKey";

        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("koddixJWT")
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;
    }
}
